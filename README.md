
   Ain Shams University
    Faculty of Engineering
Department of CSE
 
 
4th Year CSE - 1st Semester 2015/2016
CSE 421: Database Systems

Database Project

Quiz Application
PHP Web Application

Under Supervision of
Dr. Mona Ismail
Eng. Veronia 
 
 
 Project Team (G10)
Ahmed Alaa ELdin Hamdy Salem - Section 1
Ahmed Medhat - Section 1
Amr Mohamed Ragaey-  Section 2
Tarek Hossam EL Gafy- Section 2
Mohamed Yahia - Section 3



#Project description:
This application aims to facilitate  the process of making , taking , correction of quizzes,displaying result statistics. It offers an easy way for the instructor for putting multichoice questions and autocorrection of the students’ answers.The students can join any available quiz and put their answers through a web application.

#Functionalities
*Instructor
**Add a new quiz for a subject which he teaches
**Add questions to quiz
**specify as draft or published
**specify duration for the quiz
**View Statistics for published quizzes including (Maximum , minimum ,Average degree and number of students who completed the quiz
**Edit unpublished quizzes or publish it
*Student
**Solve quizzes offered to his class
**View previous results of solved quizzes

Software used
*XAMPP:
*MySQL: Database management system
*PhpMyadmin :
*Apache web server
*Bootstrap: For creating styles
*MySQL WorkBench : Aiding in creating database tables


 



Tables
primary key , forging key 
Forms & Reports
Student
login
student login by email and password


Incorrect login displays error message



Dashboard
Displays quizzes offered to student (published quizzes in subjects which he is enrolled in)

student answering a quiz
Displays the question and its available anwer
If the timer ended the solved question are submitted and the answering session is closed


Dashboard after answering the quiz
showing the answered quizs 
displayed row color  in green indicating success in quiz
displayed row color  in red indicating Failed in quiz












Instructor
login
Instructor login by email and password


Dashboard
Displays  published quizzes to student  & draft quizzes (published quizzes is quizzes published by instructor to students enrolled in his/her course  & draft quizzes is upcoming quizzes which students can not start these quizzes until own instructor publish them) , instructor also can add new quiz by press on “ + New quiz “  










Show Students who took a quiz
by pressing on “Show Students” , instructor can show list of students which solved the quiz and their results

 
Show Statistics 
by pressing on “Show Statistics” , instructor can show no. of students which solved the quiz , max & min degree and average 

Publish Draft quizzes
 draft quiz will be removed from draft quizzes list and be in Published quizzes but without “Show Statistics” &
“Show Students” buttons . in this case, students can “start” this quiz



New quiz
Instructor can put “title” for his/her quiz , select “subject” which this quiz in , determine  “duration” in mins ,
determine “number of questions” and select “status” of this quiz , if “published” students can start it immediately , but if it “draft” so it will be added to “Draft quizzes” list to “Edit” or “Publish”

now press “Next” to add “questions” , “choices”  and “correct answer”

Edit Draft quiz
draft’s quizzes are editable  - by pressing on “Edit” at any quiz - instructor can change anything in this quiz can change title,duration,subject,status and also can Edit , delete , add  any question 

 Edit Question in  update quiz section
edit question in update quiz page is also available , will see page like add “questions” page 

 Add Question in update quiz section
change “number of question” (up to add , down to delete) questions then press on 
“? add new question” to add questions
 
 



SQL statements
 Display All classes
SELECT * FROM class
Display All subjects
SELECT * FROM subject
 WHERE instructor_id={$instructor_id}
Login
For instructor
SELECT * FROM instructor 
WHERE email='$email' 
AND password='$password'"
For Student


    SELECT * FROM student 
WHERE email='$email'
 AND password='$password'"

Add new quiz
INSERT INTO quiz (time,    title,    date,        Subject_id,    published,    nuber_of_questions)
VALUES (    $duration,'$title','    $date',    $subject_id,    $status,    $number_of_questions);


Add Question and its choices
INSERT INTO question (content, right_answer, Quiz_id)
 VALUES ( $content, $correct , $quiz_id);
INSERT INTO choice (content, question_id)
VALUES ($choice1, $ question_id),
           ($choice1, $ question_id) ,($choice1, $ question_id),($choice1, $ question_id);









Display Quiz Questions
SELECT * FROM question WHERE Quiz_id = $quiz_id ;
INSERT INTO take (Student_id,Date_taken,Quiz_id,Quiz_Answer_id) 
VALUES ({$student_id},CURDATE(),{$quiz_id},{$id})
Question Choices
SELECT * FROM choice WHERE question_id = $question_id ;


Display Quizzes already taken by student
SELECT subject.name as Subject, Date_taken , result, nuber_of_questions as questions, quiz.title
 FROM quiz, take, quiz_answer, student, subject
 WHERE student.id = {$student_id} 
  and student.id = take.Student_id
  and take.Quiz_Answer_id = quiz_answer.id
  and take.Quiz_id = quiz.id
  and quiz.Subject_id = subject.id;

Submit Quiz Answer
INSERT INTO quiz_answer (result) VALUES ($result);
INSERT INTO take (Student_id,Date_taken,Quiz_id,Quiz_Answer_id)
VALUES ($student_id ,CURDATE(), $quiz_id, $id);


Display Unpublished Quizzes (Drafts)
SELECT subject.name as subject_name , class.department  , class.year, quiz.id as quiz 
from subject , class ,quiz
where subject.class_id = class.id
 and subject.id  = quiz.Subject_id
 and quiz.published = 0 
 and  subject.instructor_id = $teacher_id ;

Display Published Quizzes
SELECT subject.name as subject_name , class.department  , class.year , quiz.date, quiz.time ,quiz.id
from subject , class ,quiz
where subject.class_id = class.id 
 and subject.id  = quiz.Subject_id 
 and quiz.published = 1
 and  subject.instructor_id = $teacher_id ; 


Get Students Result in a quiz
SELECT student.name , quiz_answer.result , quiz.nuber_of_questions
from student , quiz , quiz_answer ,take
where take.Student_id = student.id
 and take.Quiz_id = quiz.id
 and take.Quiz_Answer_id = quiz_answer.id
 and quiz.id = $quiz_id ;
Display Statistics for a quiz (Max,Min, ِِAvg) results
SELECT MAX(result) , MIN(result),AVG(result)
from quiz_answer , take
where take.Quiz_Answer_id = quiz_answer.id 
and take.Quiz_id = $quiz_id ;
Display total number of students who are offered to take a quiz
from quiz , subject , class , student 
where quiz.Subject_id = subject.id
 and subject.class_id = class.id
 and student.Class_id = class.id
 and quiz.id = $quiz_id ;
Display number of students who already took a quiz
SELECT COUNT(*)  from `take` WHERE Quiz_id =  $quiz_id
Displays quizzes offered to student but not yet taken
SELECT a.quiz, a.subject, a.date, a.time , a.title
FROM (select `quiz`.`id` as quiz, student.id as student,`subject`.name as subject,`quiz`.`date` as  date,quiz.published, quiz.time, quiz.title 
FROM student,class,subject,quiz
WHERE student.id ={$student_id}
  AND student.class_id=class.id
  AND class.id = subject.class_id
  AND subject.id=quiz.Subject_id ) a

LEFT JOIN (select `quiz`.`id` as quiz, student.id as student
FROM quiz,take,student,subject
WHERE student.id = {$student_id}
  AND student.id = take.Student_id
  AND take.Quiz_id = quiz.id
  AND quiz.Subject_id = subject.id ) b

ON a.quiz = b.quiz AND a.student = b.student

WHERE b.student IS NULL AND b.quiz IS NULL AND a.published=1;


























































































