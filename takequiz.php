<?php include("layout/header.php");
$all_correct_answers=array();

?>

<div class="container">
	<div class="jumbotron">
	
	<div class="row">
		<div class="center"><h2><?php echo $_GET['name']; ?></h2></div>
		<div id="showtime" class="pull-right"></div>
	</div>
	
	<hr>
	<form method="POST" name="questionForm" action="evaluate.php?qid=<?php echo $_GET['qid']; ?>">
		<?php $i=1;$quesresult=quiz_questions($_GET['qid']);
		
			while ($quesRow=mysqli_fetch_assoc($quesresult)) :?>
		
				<p><?php echo $i.'. '.$quesRow['content']; array_push($all_correct_answers,$quesRow['right_answer']);?></p>
				<?php $choiceResult=question_choices($quesRow['id']);
				
					while($choiceRow=mysqli_fetch_assoc($choiceResult)): ?>
						<div class="radio">
		 		 			<label>
		   					 <input type="radio" name="choice_<?php echo $i;?>" id="optionsRadios1" value="<?php echo $choiceRow['content'];?>">
		   					 <?php echo $choiceRow['content']; ?>
		 					</label>
		 				</div>
					<?php endwhile;?>
		<?php $i++;endwhile;?>

						<input type="hidden" name="minute"/> 
				<input type="hidden" name="second"/>
				<input type="hidden" name="ss"/>

		<div class="row">
			<div class="center">
				<button onCLick="customSubmit()" class="btn btn-success center" name="ss">Submit</button>
			</div>
		</div>
	</form>

	<br>
	<?php
	$_SESSION['answers']=$all_correct_answers;
	$_SESSION['degree']=$i-1;
	?>
	
	</div>
</div>

<script type="text/javascript">
    var tim;       
    var min = parseInt("<?php echo $_GET['t']; ?>");
    var sec = 0;
    var f = new Date();

    function customSubmit(){
    	document.forms[0].submit();
    }

    function examTimer() {
        if (parseInt(sec) >0) {

		    document.getElementById("showtime").innerHTML = "Time Remaining :"+min+" Minutes ," + sec+" Seconds";
            sec = parseInt(sec) - 1;                
            tim = setTimeout("examTimer()", 1000);
        }
        else {

		    if (parseInt(min)==0 && parseInt(sec)==0){
		    	document.getElementById("showtime").innerHTML = "Time Remaining :"+min+" Minutes ," + sec+" Seconds";
			     // alert("Time Up");
			     document.questionForm.minute.value=0;
			     document.questionForm.second.value=0;
			     document.forms[0].submit();

		     }

            if (parseInt(sec) == 0) {				
			    document.getElementById("showtime").innerHTML = "Time Remaining :"+min+" Minutes ," + sec+" Seconds";					
                min = parseInt(min) - 1;
				sec=59;
                tim = setTimeout("examTimer()", 1000);
            }

        }
    }

    examTimer();
</script>

<?php include ('layout/footer.php'); ?>

