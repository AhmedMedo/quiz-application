<?php

// in this we will include the files of functions and check if the user is instructor
// we will assume the user is logged in
//include('includes/functions.php');
//dbconnect("localhost", "quizuser", "quiz", "quiz");
 include("layout/adminheader.php");


// in add press the data will be saved and send to database
if(isset($_POST['add']))
{
 // save it to database with the current quiz id
// unset($_SESSION['question_error']);
 $content=$_POST['content'];
 $correct=$_POST['correct_answer'];
 $choice_1=$_POST['choice_1'];
 $choice_2=$_POST['choice_2'];
 $choice_3=$_POST['choice_3'];
 $choice_4=$_POST['choice_4'];
 //$choices=array();
 //print_r($_POST);
 if($correct==1){$correct=$choice_1;}

 if($correct==2){$correct=$choice_2;}

 if($correct==3){$correct=$choice_3;}

 if($correct==4){$correct=$choice_4;}

if(isset($_GET['qid']))
{
	$quiz_id=$_GET['qid'];
	add_question_to_quiz($quiz_id,$content,$correct,$choice1,$choice2,$choice3,$choice4);
	redirect_to('admin.php');
}
else
{
  add_question($content,$correct,$choice_1,$choice_2,$choice_3,$choice_4);


 //array_push($choices, $choice1,$choice2,$choice3,$choice4);
 //print_r($choices);

 $_SESSION['counter']++;


 if($_SESSION['counter'] > $_SESSION['num_of_ques'])

 {
  redirect_to('message.php');
 }
}
}
if(isset($_POST['finish']))
{
unset($_SESSION['counter']);
unset($_SESSION['num_of_ques']);
unset($_SESSION['question_error']);
redirect_to('admin.php');
}


?>

<div class="container">

<?php if(isset($_GET['qid'])){?>
   <form method="POST" action="question.php?qid=<?php echo $_GET['qid'];?>">
 <?php }else{?>
   <form method="POST" action="question.php">

 <?php }?>
  <div class="form-group">
    <label for="exampleInputEmail1">Question <?php echo $_SESSION['counter'];?></label>
      <textarea class="form-control" rows="5" name="content"></textarea>

  </div>
  




<div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 1 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice one" name="choice_1" required> 
      </div>
    </div>
 </div>
 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 2 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice two"name="choice_2" required> 
      </div>
    </div>
 </div>
 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 3 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice three" name="choice_3"required>
      </div>
    </div>
 </div>
 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 4 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice Four" name="choice_4" required> 
      </div>
    </div>
 </div>

 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Correct answer</label>
      </div>
      
      <div class="col-sm-5">
      <select name="correct_answer" class="form-control" required>
      <option value="1">choice 1</option>
      <option value="2">choice 2</option>
      <option value="3">choice 3</option>
      <option value="4">choice 4</option>


</select>
      </div>
    </div>
 </div>
  
  <br>
  <div class="row">
  <div class="col-sm-5">
  <button type="submit" class="btn btn-primary" name="add">Add A new one</button>
  </div>
   <div class="col-sm-5">
  <button type="submit" class="btn btn-primary" name="finish">Finish</button>
  </div>
  </div>
</form>

</div>

  <?php include ('layout/footer.php'); ?>