<?php
// in this we will include the files of functions and check if the user is instructor
// we will assume the user is logged in
//include('includes/functions.php');
//echo $_SESSION['quiz_id'];

 include("layout/adminheader.php");

    if(isset($_SESSION['class_id'])) 
    {
      redirect_to('student.php');
    }

    if(isset($_POST['publish_quiz'])) 
    {
      set_publish($_GET['publish']);
        redirect_to('admin.php');

    }
    if(isset($_POST['delete_quiz']))
    {
    	delete_quiz($_GET['delete']);
    	redirect_to('admin.php');
    }
//dbconnect("localhost", "quizuser", "quiz", "quiz");

?>

<div class="container">
<!-- Button trigger modal -->

		  <div class="published_puizzes">
		    <h2> <i class="fa fa-eye fa-lg fa-fw"></i>Published Quizzes</h2>
		    <table class="table table-hover">
		     <thead>
		        <tr>
		          <th>Date</th>
		          <th>Title</th>
		          <th>Subject</th>
		          <th>Department</th>
		          <th>Duration</th>
		          <th>Students</th>
		          <th>Statistics</th>
		        </tr>
		      </thead>
		      <tbody>
 		         
 		         <?php $result=published_quizzes($_SESSION['user_id']);
		          while ($row=mysqli_fetch_assoc($result)) :?>
		          
		          <tr>
 		            <td><?php echo $row['date'];?></td>
 		            <td><?php echo $row['title'];?></td>
		            <td><?php echo $row['subject_name'];?></td>
		            <td><?php echo $row['department']." ".$row['year'];?></td>
		            <td><?php echo $row['time'];?> Mins.</td>
		            <td>

		            <?php if(mysqli_num_rows(get_students_result($row['id'])) == 0){ ?>
		            	No student takes this quiz
		            <?php } else {?>
		            <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal_<?php echo $row['id'];?>">
							 Show students
							</button>

							<!-- Modal -->
						<div class="modal fade" id="myModal_<?php echo $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">List of students</h4>
									</div>
									
									<div class="modal-body">
									<table class="table table-hover">
									<thead>
										<th>student name</th>
										<th>result</th>
									</thead>

										<?php $student_result=get_students_result($row['id']); while($student_row=mysqli_fetch_assoc($student_result)): ?>
										<tr>
											<td><?php  echo $student_row['name'];?></td>
											<td><?php  echo $student_row['result']."/".$student_row['nuber_of_questions']; ?></td>
										</tr>
									<?php endwhile;?>
									</tbody>
									</table>
									</div>
									
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<?php }?>
		            </td>

		            <td>
			            <?php if(mysqli_num_rows(get_students_result($row['id'])) == 0){ ?>
			            	Not Available
			            <?php } else {?>
			            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal_<?php echo $row['id'].''.$row['subject_name'];?>">
							Show Statistics
						</button>

							<!-- Modal -->
						<div class="modal fade" id="myModal_<?php echo $row['id'].''.$row['subject_name'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">Quiz Statistics</h4>
									</div>
									
									<div class="modal-body">
										<?php $t = num_total_students_in_quiz($row['id']);
											$val1 = mysqli_fetch_assoc($t);
											$n = num_students_took_quiz($row['id']);
											$val2 = mysqli_fetch_assoc($n);
											$val =  round($val2['c']/$val1['c']*100,2);

											echo '<h5><b>'.$val2['c'].''." students of ".''.$val1['c'].''." took this quiz.</b><br><h5>"
										 ?>
										<div class="progress">
											<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $val; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $val; ?>%;">
												<?php echo $val.''."%"; ?>
											</div>
										</div>
										<?php
											$mma = max_min_avg($row['id']);
											$value = mysqli_fetch_assoc($mma);
											$max = $value['max'];
											$min = $value['min'];
											$avg = round($value['avg'],2);
										?>
										<h5><b> Max Student Degree : <?php echo $max; ?></b></h5>
										<h5><b> Min Student Degree : <?php echo $min; ?></b></h5>
										<h5><b> Avg Degree of Students : <?php echo $avg; ?></b></h5>

									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<?php }?>
		            </td>
		          </tr>

 		        <?php endwhile;?>
		      </tbody>

		    </table>
		  </div>


		  <div class="Unpublished_Quizzes">
		    <h2> <i class="fa fa-eye-slash fa-lg fa-fw"></i>Draft Quizzes</h2>
		    <?php if(mysqli_num_rows(unpublished_quizzes($_SESSION['user_id'])) == 0){?>
		    	<i>There's no draft quizzes</i>
		    <?php } else{?>
		    <table class="table table-hover">
		     <thead>
		        <tr>
		        	<th>Title</th>
		          <th>Subject</th>
		          <th>Department</th>
		          <th>Publish</th>
		          <th>Edit</th>
		          <th>Delete</th>

		        </tr>
		      </thead>
		      <tbody>
		         <?php $result=unpublished_quizzes($_SESSION['user_id']);

		          while ($row=mysqli_fetch_assoc($result)) : ?>     
		          <tr>
		          	<td><?php echo $row['title'];?></td>
		            <td><?php echo $row['subject_name'];?></td>
		            <td><?php echo $row['department']." ".$row['year'];?></td>
					<td>
					<form method="post" action="admin.php?publish=<?php echo $row['quiz']; ?>"><input type="submit" name="publish_quiz" class="btn btn-primary" value="Publish"/></form></td>
					<td><form method="post" action="edit.php?edit=<?php echo $row['quiz']; ?>"><input type="submit" name="edit_quiz" class="btn btn-success" value="Edit"/></form></td>
					<td><form method="post" action="admin.php?delete=<?php echo $row['quiz']; ?>"><input type="submit" name="delete_quiz" class="btn btn-danger" value="Delete"/></form></td>

		          </tr>
		        <?php endwhile;?>
		      </tbody>
		    </table>
		    <?php }?>
		  </div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php include ('layout/footer.php'); ?>