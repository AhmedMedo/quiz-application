-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2015 at 02:16 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `choice`
--

CREATE TABLE IF NOT EXISTS `choice` (
  `id` int(11) NOT NULL,
  `content` varchar(45) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `choice`
--

INSERT INTO `choice` (`id`, `content`, `question_id`) VALUES
(1, '2', 1),
(2, '3', 1),
(3, '4', 1),
(4, 'select', 2),
(5, 'get', 2),
(6, 'retrive', 2),
(7, 'hamada', 4),
(8, 'agregate', 3),
(9, 'intrinsic', 3),
(10, 'inline', 3),
(11, 'poliphormic', 3),
(12, 'Elmasri', 4),
(13, 'Knuth', 4),
(14, 'stalling', 4),
(15, 'ali', 4),
(21, '1200', 7),
(22, '1201', 7),
(23, '1000', 7),
(24, '4000', 7),
(25, 'CDC', 8),
(26, 'VDC', 8),
(27, 'CCC', 8),
(28, 'cadace', 8),
(29, 'Block level file system', 9),
(30, 'File level', 9),
(31, 'Network level', 9),
(32, 'Disk level', 9),
(33, 'CVS', 10),
(34, 'UML', 10),
(35, 'flow chart', 10),
(36, 'EBNF', 10),
(37, 'The project is behind schedule', 11),
(38, 'The project is under budget', 11),
(39, 'The project performance is bad', 11),
(40, 'The project performance is good', 11),
(41, 'specialization partial, disjoint', 12),
(42, 'specialization total , overlapping', 12),
(43, 'specialization total , disjoint', 12),
(44, 'specialization partial, overlapping', 12),
(45, 'Foreign key approach', 13),
(46, 'Merged relation approach', 13),
(47, '(Foreign Key) or (Merged relation approach )', 13),
(48, 'Cross-reference', 13),
(61, 'Extenal', 17),
(62, 'Internal', 17),
(63, 'conceptual', 17),
(64, 'none of the above', 17),
(65, 'Internal', 18),
(66, 'External', 18),
(67, 'conceptual', 18),
(68, 'none of the above', 18),
(69, 'Internal', 19),
(70, 'External', 19),
(71, 'conceptual', 19),
(72, 'none of the above', 19);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` int(11) NOT NULL,
  `department` varchar(45) NOT NULL,
  `year` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `department`, `year`, `name`) VALUES
(1, 'CSE', 2016, ''),
(2, 'CSE', 2017, ''),
(3, 'EPM', 2016, '');

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE IF NOT EXISTS `instructor` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`id`, `name`, `email`, `password`) VALUES
(101, 'Ali', 'Ali@gmail.com', 'ali'),
(102, 'Sara', 'Sara@hotmail.com', 'sara'),
(103, 'Amr', 'Amr@gmail.com', 'amr'),
(104, 'mona', 'mona@gmail.com', 'mona'),
(105, 'gamal', 'gamal@gmail.com', 'gamal'),
(106, 'hazem', 'hazem@gmail.com', 'hazem'),
(107, 'ayman', 'ayman@gmail.com', 'ayman');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `right_answer` varchar(45) DEFAULT NULL,
  `Quiz_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `content`, `right_answer`, `Quiz_id`) VALUES
(1, 'what the greatest common division between 10 and 6 ?', '2', 1),
(2, 'to retrieve data from sql key word used', 'select', 2),
(3, 'AVG , SUM .. arre ___________ functions', 'agregate', 2),
(4, 'author of reference', 'Elmasri', 2),
(7, 'Number of pages of reference', '1201', 3),
(8, 'classical Data center acronomy', 'CDC', 4),
(9, 'SAN is', 'Block level file system', 4),
(10, 'For software design graphs we use?', 'UML', 5),
(11, 'Given the following information for a two-year project: (Final Exam Jan. 2014)\r\nPresent Value   = 120,000 L.E.\r\nEarned Value    = 130,000 L.E.\r\nActual Cost    = 150,000 L.E.\r\nBudget at Completion = 500,000 L.E.', 'The project is behind schedule.', 5),
(12, 'Mapping Specialization or Generalization using Option 8B: Multiple relations—subclass relations only:', 'specialization total , disjoint', 3),
(13, 'Mapping of Binary M:N Relationship Types we use', 'Cross-reference', 3),
(17, 'Schema which describes the structure of the whole database for a community of users', 'conceptual', 12),
(18, ' schema which describes the physical storage structure of the database', 'Internal', 12),
(19, 'schema describes the part of the database that a particular user group is interested in and hides the rest of the database from that\r\nuser group', 'External', 12);

-- --------------------------------------------------------

--
-- Table structure for table `question_answer`
--

CREATE TABLE IF NOT EXISTS `question_answer` (
  `id` int(11) NOT NULL,
  `Result` tinyint(1) DEFAULT NULL,
  `Quiz_Answer_id` int(11) NOT NULL,
  `Answer` int(11) DEFAULT NULL,
  `Question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `time` varchar(45) NOT NULL,
  `date` date NOT NULL,
  `Subject_id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '0',
  `nuber_of_questions` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `title`, `time`, `date`, `Subject_id`, `published`, `nuber_of_questions`) VALUES
(1, 'Operations quiz', '', '2015-12-01', 1, 1, 1),
(2, 'SQL quiz', '60', '2015-12-26', 2, 1, 2),
(3, 'SQL 2 quiz', '3', '2015-12-02', 2, 1, 3),
(4, 'Storage quiz', '60', '2015-12-26', 4, 1, 2),
(5, 'Team management quiz', '60', '2015-12-16', 6, 1, 2),
(6, 'Data link quiz', '60', '2015-12-26', 8, 1, 2),
(7, 'Team management quiz 2', '60', '2015-12-26', 6, 1, 2),
(8, 'AES quiz', '60', '2015-12-26', 5, 1, 2),
(9, 'AES quiz 2', '60', '2015-12-26', 5, 0, 2),
(10, 'AES quiz 3', '60', '2015-12-26', 5, 0, 2),
(12, 'Quiz on introduction', '10', '2015-12-26', 2, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE IF NOT EXISTS `quiz_answer` (
  `id` int(11) NOT NULL,
  `result` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_answer`
--

INSERT INTO `quiz_answer` (`id`, `result`) VALUES
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 2),
(24, 0),
(27, 3),
(28, 0),
(29, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `Class_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `email`, `password`, `Class_id`) VALUES
(1, 'Ahmed', 'ahmed@gmail.com', 'ahmed', 1),
(2, 'tarek', 'tarek@gmail.com', 'tarek', 2),
(3, 'Yehia', 'Yehia@gmail.com', 'yehia', 3),
(6, 'ayman', 'ayman@gmail.com', 'ayman', 1),
(7, 'ali', 'ali@gmail.com', 'ali', 1),
(8, 'medo', 'medo@gmail.com', 'medo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `class_id`, `instructor_id`) VALUES
(1, 'Math', 1, 101),
(2, 'Database', 1, 104),
(3, 'Physics', 3, 103),
(4, 'Distributed Systems', 1, 105),
(5, 'Security', 1, 106),
(6, 'Project Management', 2, 105),
(7, 'Micro processor', 2, 107),
(8, 'Networks', 1, 107);

-- --------------------------------------------------------

--
-- Table structure for table `take`
--

CREATE TABLE IF NOT EXISTS `take` (
  `Student_id` int(11) NOT NULL,
  `Date_taken` date DEFAULT NULL,
  `Quiz_id` int(11) NOT NULL,
  `Quiz_Answer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `take`
--

INSERT INTO `take` (`Student_id`, `Date_taken`, `Quiz_id`, `Quiz_Answer_id`) VALUES
(1, '2015-12-25', 1, 24),
(7, '2015-12-26', 3, 27),
(8, '2015-12-26', 3, 29),
(1, '2015-12-25', 4, 23),
(7, '2015-12-26', 4, 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choice`
--
ALTER TABLE `choice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_choice_question1_idx` (`question_id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_Question_Quiz1_idx` (`Quiz_id`);

--
-- Indexes for table `question_answer`
--
ALTER TABLE `question_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Question_Answer_Quiz_Answer1_idx` (`Quiz_Answer_id`),
  ADD KEY `fk_Question_Answer_Question1_idx` (`Question_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_quiz_Subject1_idx` (`Subject_id`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_Student_Class_idx` (`Class_id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Subject_class1_idx` (`class_id`),
  ADD KEY `fk_Subject_instructor1_idx` (`instructor_id`);

--
-- Indexes for table `take`
--
ALTER TABLE `take`
  ADD PRIMARY KEY (`Quiz_id`,`Student_id`),
  ADD KEY `id_idx` (`Quiz_Answer_id`),
  ADD KEY `id_idx1` (`Quiz_id`),
  ADD KEY `student_id` (`Student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `choice`
--
ALTER TABLE `choice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `choice`
--
ALTER TABLE `choice`
  ADD CONSTRAINT `fk_choice_question1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `fk_Question_Quiz1` FOREIGN KEY (`Quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `question_answer`
--
ALTER TABLE `question_answer`
  ADD CONSTRAINT `fk_Question_Answer_Question1` FOREIGN KEY (`Question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Question_Answer_Quiz_Answer1` FOREIGN KEY (`Quiz_Answer_id`) REFERENCES `quiz_answer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `fk_quiz_Subject1` FOREIGN KEY (`Subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `fk_Student_Class` FOREIGN KEY (`Class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `fk_Subject_class1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Subject_instructor1` FOREIGN KEY (`instructor_id`) REFERENCES `instructor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `take`
--
ALTER TABLE `take`
  ADD CONSTRAINT `Quiz_answer` FOREIGN KEY (`Quiz_Answer_id`) REFERENCES `quiz_answer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `Quiz_id` FOREIGN KEY (`Quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_id` FOREIGN KEY (`Student_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
