<?php
// in this we will include the files of functions and check if the user is instructor
// we will assume the user is logged in
//include('includes/functions.php');
//echo $_SESSION['quiz_id'];

 include("layout/adminheader.php");

    if(isset($_SESSION['class_id'])) 
    {
      redirect_to('student.php');
    }
//dbconnect("localhost", "quizuser", "quiz", "quiz");
if(isset($_POST['next']))
{
  $subject_id=$_POST['subject_id'];
  $title=$_POST['title'];
  $duration=$_POST['duration'];
  $number_of_questions=$_POST['num_of_ques'];
  $status=$_POST['status'];
 // print_r($_POST);
  $_SESSION['counter']=1;
  $_SESSION['num_of_ques']=$number_of_questions;
  add_quiz($subject_id,$title,$duration,$number_of_questions,$status);
  redirect_to('question.php?qn='.$_POST['num_of_ques']);

}

?>

<div class="container">

    <h1>New quiz</h1>
    <form class="form-horizontal" method="POST" action="">
    <div class="form-group">
         <label  class="col-sm-2 control-label">title</label>
         <div class="col-sm-5">
         <input type="text" name="title" class="form-control">
         </div>


    </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">quiz subject</label>
    <div class="col-sm-5">
    <select class="form-control" name="subject_id">
      
   
    <?php 
      $result_set=all_subjects($_SESSION['user_id']);
      while ($row=mysqli_fetch_assoc($result_set)): 
        ?>      
      <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
    <?php  endwhile;?>
      
    
     </select>
    </div>
  </div>

  <div class="form-group">
  <label class="col-sm-2 control-label">Duration</label>
  <div class="col-sm-5">
    <input type="number" class="form-control" name="duration">
  </div>
</div>

  
  <div class="form-group">
  <label class="col-sm-2 control-label"> Number of question</label>
    <div class="col-sm-5"> 
      <input type="number" class="form-control" name="num_of_ques">

    </div>
    
  </div>

  <div class="form-group">
  <label class="col-sm-2 control-label">status</label>
  <div class="col-sm-5">
  <select class="form-control" id="sel1" name="status">
    <option value="1">publish now</option>
    <option  value="0">draft</option>


  
  </select>
  </div>
</div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="next">Next</button>
    </div>
  </div>
</form>
</div>

  <?php include ('layout/footer.php'); ?>