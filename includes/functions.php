<?php

session_start();

dbconnect("localhost", "quizuser", "quiz", "quiz");

function redirect_to($new_location)
	{
		header("Location:". $new_location);
		exit;
	}

/* here we will write all funtions and sql quires */

// function add_new_question($content,$right_answer, $quiz_id){
   
//   global $con;
 
// $query = "INSERT into question (content,right_answer,Quiz_id) values ('$content',$right_answer,$quiz_id) ;" ;
//  $result = @mysql_query($query,$con) or die (mysql_error()) ;
//   $id = mysql_insert_id();
//  return $id;   
   
// }
/* @param none
	*return id of all classes
*/

function all_classes()
{
	global $connection;
	$sql="SELECT * FROM class";
	$result=mysqli_query($connection,$sql);
	return $result;
}

function all_subjects($instructor_id)
{
	global $connection;
	$sql="SELECT * FROM subject WHERE instructor_id={$instructor_id}";
	$result=mysqli_query($connection,$sql);
	return $result;
}

/*======================= Ahmed Medhat ================================*/
function dbconnect($dbhost, $dbuser, $dbpass, $dbname)
{ global $connection;
  $connection = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
  if(mysqli_connect_errno()){
	  die("Database connection failed:".mysqli_connect_error()."(".mysqli_connect_errno().")");
	}
}
	
 //close connection
function closeconnect()
{
	global $connection;
 	mysqli_close($connection);
}

//login
function login($email,$password,$type)
{
	global $connection;
	if($type == "instructor")
	{	$query ="SELECT * FROM instructor WHERE email='$email' AND password='$password'";}
	if($type == "student")
	
	{   $query ="SELECT * FROM student WHERE email='$email' AND password='$password'";}
		  
	$result =mysqli_query($connection,$query);
	if(!$result)
	{
		// user not found
		$_SESSION['login_error']="Invalid user ! please check your username and password";
		redirect_to('index.php');

	}	

		$row = mysqli_fetch_assoc($result);
		$_SESSION['user_id']=$row["id"];
		$_SESSION['username']=$row["name"];
		if($type=="student"){$_SESSION['class_id']=$row['Class_id'];}
		if($type == "instructor"){unset($_SESSION['login_error']);redirect_to('admin.php');}	
		else{unset($_SESSION['login_error']);redirect_to('student.php');}
	

	// if($result){echo "success";}
	// else{die("Database query failed" .mysqli_error($connection));} 
	// $row = mysqli_fetch_assoc($result);
	// if($row["id"]){
	// 	//setcookie("user_id",$row["id"],time()+(60*60*24*7));
	// 	$_SESSION['user_id']=$row["id"];
	// 	$_SESSION['username']=$row["name"];
	// 	if($type=="student"){$_SESSION['class_id']=$row['Class_id'];}
	// 	if($type == "instructor"){redirect_to('admin.php');}	
	// 	else{redirect_to('student.php');}
	// }
	// else{$_SESSION['login_error']="Invalid user ! please check your username and password";}
}
	
 //add new quiz
function add_quiz($subject_id,$title,$duration,$number_of_questions,$status,$classes)
{   
	global $connection;
	//if post is draft leave the date empy
	//if publish now save date of now
	$instructor_id = $_SESSION['user_id'];

	if($status==1)
	{
		$date=date("Y-m-d");
	}
	if($status==0)
	{
		$date=NULL;
	}
	$query = "INSERT INTO quiz (time,title,date,Subject_id,published,nuber_of_questions)
			  VALUES ({$duration},'{$title}','{$date}',{$subject_id},{$status},{$number_of_questions})";
	

	  
	$result =mysqli_query($connection,$query);
	if($result){
	  $last_id = mysqli_insert_id($connection);
	 // echo "success";
	}
	else{die("Database query failed" .mysqli_error($connection));} 
	  
	//setcookie("quiz_id",$last_id,time()+(60*60*24*7));  
	$_SESSION['quiz_id']=$last_id;
	/*=============== Medo section *=================*/
	//the instructor will choose the class by himself 	
}
 
 //add question
 function add_question($content,$correct,$choice1,$choice2,$choice3,$choice4)
 {  
 	global $connection;
	$quiz_id = $_SESSION['quiz_id'];
	$query = "INSERT INTO question (content,right_answer,Quiz_id) VALUES ('{$content}','{$correct}',{$quiz_id})";
	  
	  $result =mysqli_query($connection,$query);
	  if($result){
	    $last_id = mysqli_insert_id($connection);
	    //echo "success";
	}
	  else{die("Database query failedrrr" .mysqli_error($connection));}
	  
	  
      $query = "INSERT INTO choice (content, question_id)
				VALUES ('{$choice1}', {$last_id}),
				       ('{$choice2}', {$last_id}),        
					   ('{$choice3}', {$last_id}),
					   ('{$choice4}', {$last_id}) ";
	   
	$result =mysqli_query($connection,$query);
		if($result)
		{//echo "success";
		}
		else{ die("Database query failed" .mysqli_error($connection));}
}

function quiz_questions($quiz_id)
{
 
 	global $connection;
 	// first get class id that has this quiz form 
 	$sql="SELECT * FROM question WHERE Quiz_id={$quiz_id}";
 	$result=mysqli_query($connection,$sql);
 	if(!$result)
 	{
 		die("Database query failed of questions" .mysqli_error($connection));
 	}
 	return $result;// assoc array of all questions of that quiz
}


function question_choices($question_id)
 {
 	global $connection;
 	// first get class id that has this quiz form 
 	$sql="SELECT * FROM choice WHERE question_id={$question_id}";
 	$result=mysqli_query($connection,$sql);
 	if(!$result)
 	{
 		die("Database query failed of choices" .mysqli_error($connection));
 	}
 	return $result;// assoc array of all choices of a given question
 }


function past_quizzes($student_id){
	global $connection;

	$query = "SELECT subject.name as Subject, Date_taken , result, nuber_of_questions as questions, quiz.title 
			FROM quiz, take, quiz_answer, student, subject
			WHERE student.id = {$student_id} and student.id = take.Student_id 
			and take.Quiz_Answer_id = quiz_answer.id 
			and take.Quiz_id = quiz.id
			and quiz.Subject_id = subject.id;";

	$result = @mysqli_query($connection,$query);
	return $result ;
}

function submit_quiz_answer($student_id,$quiz_id,$result){
   	global $connection;
	$sql_one="INSERT INTO quiz_answer (result) VALUES ({$result})";
	$result_one=mysqli_query($connection,$sql_one);
	$id= mysqli_insert_id($connection);
	$sql_two="INSERT INTO take (Student_id,Date_taken,Quiz_id,Quiz_Answer_id) VALUES ({$student_id},CURDATE(),{$quiz_id},{$id})";
	$result_two=mysqli_query($connection,$sql_two);
}
 
/* 
function add_new_question($content,$right_answer, $quiz_id){
  	global $connection;
	$query = "INSERT into question (content,right_answer,Quiz_id) values ('$content',$right_answer,$quiz_id) ;" ;
	 $result = @mysql_query($query,$con) or die (mysql_error()) ;
	  $id = mysql_insert_id();
 return $id;   
}

*/


 function unpublished_quizzes($teacher_id){
   	global $connection;


	$sql="SELECT subject.name as subject_name , class.department  , class.year, quiz.id as quiz, quiz.title
	from subject , class ,quiz
	where subject.class_id = class.id
	and subject.id  = quiz.Subject_id
	and quiz.published = 0 
	and  subject.instructor_id = $teacher_id ;";

	$result=mysqli_query($connection,$sql) or die (mysqli_error());
	return $result ; // col0 : subject , col1 : department , col2 : year 
}

function published_quizzes($teacher_id){
   	global $connection;

	$sql="SELECT subject.name as subject_name , class.department  , class.year , quiz.date, quiz.time ,quiz.id, quiz.title
	from subject , class ,quiz
	where subject.class_id = class.id
	and subject.id  = quiz.Subject_id
	and quiz.published = 1 
	and  subject.instructor_id = $teacher_id ;";

	$result=mysqli_query($connection,$sql);
	return $result ; // col0 : subject , col1 : department , col2 : year  , col3 : date
	
}

function set_publish($quiz_id ){
   	global $connection;

	$sql1 ="UPDATE quiz SET date = CURDATE() WHERE id =$quiz_id";
	mysqli_query($connection,$sql1);
	$sql2="UPDATE quiz
     SET published=1
     WHERE id=$quiz_id ;";

	$result=mysqli_query($connection,$sql2);
	return $result ; 
}

function get_students_result($quiz_id){
	global $connection;

	$sql = "SELECT student.name , quiz_answer.result , quiz.nuber_of_questions 
	from student , quiz , quiz_answer ,take
	where take.Student_id = student.id 
	and take.Quiz_id = quiz.id 
	and take.Quiz_Answer_id = quiz_answer.id 
	and quiz.id = $quiz_id ; ";

	$result = mysqli_query($connection,$sql);
	return $result ; // col0 : student name , col1 : his quiz result , col2 : num of questions	  
}
  
function max_min_avg($quiz_id){
	global $connection;

	$sql = "SELECT MAX(result) as max , MIN(result) as min , AVG(result) as avg
	from quiz_answer , take
	where take.Quiz_Answer_id = quiz_answer.id
	and take.Quiz_id = $quiz_id ;";

	$result = mysqli_query($connection,$sql);
	return $result; //col0 : max , col1: min , col2:avg
}

function num_total_students_in_quiz($quiz_id){
	global $connection;
	$sql = "SELECT COUNT(*) as c
	from quiz , subject , class , student
	where quiz.Subject_id = subject.id 
	and subject.class_id = class.id 
	and student.Class_id = class.id
	and quiz.id = $quiz_id ; " ;
	$result = mysqli_query($connection,$sql);
	return $result;
}


function num_students_took_quiz($quiz_id){
	global $connection;
	$sql = "SELECT COUNT(*) as c  from `take` WHERE Quiz_id =  $quiz_id  ;";
	$result = mysqli_query($connection,$sql);
	return $result;
}

 
function get_all_student_results($quiz_id)
 {
	 //quiz_id refer to published & solved quiz 
	global $connection;

	$sql="SELECT student_id,quiz_answer_id FROM take WHERE Quiz_id={$quiz_id}";
	$rst =mysqli_query($connection,$sql);

	if($rst){/*success*/}
		else{die("Database query failed" .mysqli_error($connection));}

	while($rw = mysqli_fetch_assoc($rst))
	    {
		
			$sql1="SELECT name FROM student WHERE id={$rw["student_id"]}";
			$rst1 =mysqli_query($connection,$sql1);
		    $sql2="SELECT result FROM quiz_answer WHERE id={$rw["quiz_answer_id"]}";
	        $rst2 =mysqli_query($connection,$sql2);
			$a=array();					
			while(($rw1 = mysqli_fetch_assoc($rst1)) && ($rw2 = mysqli_fetch_assoc($rst2)))
			{
				$x= $rw1["name"]." = ".$rw2["result"];
				array_push($a,$x);	
			}
	    }
		
	return $a; 
}
 

function unsolved_quizzes($student_id){
 	global $connection;
 	// first get class id that has this quiz form 
	$sql="SELECT a.quiz, a.subject, a.date, a.time , a.title
		FROM (select `quiz`.`id` as quiz, student.id as student,`subject`.name as subject,`quiz`.`date` as date,quiz.published, quiz.time, quiz.title
				FROM student,class,subject,quiz 
				WHERE 
					student.id ={$student_id} AND
					student.class_id=class.id AND
					class.id = subject.class_id AND
					subject.id=quiz.Subject_id 
					) a 
		      	LEFT JOIN (select `quiz`.`id` as quiz, student.id as student
							FROM quiz,take,student,subject
							WHERE student.id = {$student_id} AND 
							student.id = take.Student_id AND 
							take.Quiz_id = quiz.id AND 
							quiz.Subject_id = subject.id ) b
				ON a.quiz = b.quiz AND a.student = b.student
		WHERE b.student IS NULL AND b.quiz IS NULL AND a.published=1;";

 	$result=mysqli_query($connection,$sql);
 	if(!$result)
 	{
 		die("Database query failedddddd" .mysqli_error($connection));
 	}
 	return $result;
 }

 function delete_quiz($quiz_id)
 {
 	global $connection;
 	$sql="DELETE FROM quiz WHERE id={$quiz_id}";
 	$result=mysqli_query($connection,$sql);
 	if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));
 	}
 	return $result;
 }
 function get_quiz($quiz_id)
 {
 	global $connection;
 	$sql="SELECT * FROM quiz WHERE id={$quiz_id}";
 	$result=mysqli_query($connection,$sql);
 	if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	$row=mysqli_fetch_assoc($result);
 	return $row;
 }

 function update_quiz($quiz_id,$subject_id,$title,$duration,$number_of_questions,$status)
 {
 	global $connection;
 	if($status==1)
	{
		$date=date("Y-m-d");
	}
	if($status==0)
	{
		$date=NULL;
	}
 	$sql ="UPDATE quiz SET title='{$title}',time='{$duration}',date='{$date}',Subject_id={$subject_id},published={$status},nuber_of_questions={$number_of_questions} WHERE id={$quiz_id}";
 	$result=mysqli_query($connection,$sql);
 	if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;
 }


function questions_of_quiz($quiz_id)
{ 
	global $connection;
	$sql="SELECT * FROM question WHERE Quiz_id={$quiz_id}";
	$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;
}

function choices_of_question($question_id)
{
	global $connection;
	$sql="SELECT * FROM choice WHERE question_id={$question_id}";
	$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;
}

function delete_question($question_id)
{
	global $connection;
	$sql="DELETE FROM question WHERE id={$question_id}";
		$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;


}
function update_question($content,$correct,$question_id)
{
	global $connection;
	$sql="UPDATE question SET content='{$content}',right_answer='{$correct}' WHERE id={$question_id}";
	$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;

}
function  update_choice($choice_id,$choice,$question_id)
{
	global $connection;
	$sql="UPDATE choice SET content='{$choice}' WHERE question_id={$question_id} AND id={$choice_id}";
	$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;
}

function get_question($question_id)
{
	global $connection;
	$sql="SELECT * FROM question WHERE id={$question_id}";
	$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	$row=mysqli_fetch_assoc($result);
 	return $row;
}

function get_choices($question_id)
{
	global $connection;
	$sql="SELECT * FROM choice WHERE question_id={$question_id}";
	$result=mysqli_query($connection,$sql);
		if(!$result)
 	{
 		die("Database query failed" .mysqli_error($connection));

 	}
 	return $result;


}
function add_question_to_quiz($quiz_id,$content,$correct,$choice1,$choice2,$choice3,$choice4)
 {  
 	global $connection;
	$query = "INSERT INTO question (content,right_answer,Quiz_id) VALUES ('{$content}','{$correct}',{$quiz_id})";
	  
	  $result =mysqli_query($connection,$query);
	  if($result){
	    $last_id = mysqli_insert_id($connection);
	    //echo "success";
	}
	  else{die("Database query failedrrr" .mysqli_error($connection));}
	  
	  
      $query = "INSERT INTO choice (content, question_id)
				VALUES ('{$choice1}', {$last_id}),
				       ('{$choice2}', {$last_id}),        
					   ('{$choice3}', {$last_id}),
					   ('{$choice4}', {$last_id}) ";
	   
	$result =mysqli_query($connection,$query);
		if($result)
		{//echo "success";
		}
		else{ die("Database query failed" .mysqli_error($connection));}
}
?>
