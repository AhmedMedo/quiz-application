<?php include("layout/header.php");

if(isset($_POST['ss']))
{
	$studentResult=$_SESSION['degree']-count(array_diff($_SESSION['answers'],$_POST));
	submit_quiz_answer($_SESSION['user_id'],$_GET['qid'],$studentResult);
	$total = $studentResult / count($_SESSION['answers']) * 100;
}
?>

<div class="container">
<div id="result">
</div>
<a id="link" href="student.php">Back to quizzes</a>
</div>

<script type="text/javascript">
var result = document.getElementById("result");
var total = parseInt("<?php echo $total; ?>");
if(total > 50) {
	result.className += "alert alert-success";
	result.innerHTML = "<strong>Success!</strong> Congratulations, You have got score "+total+"%"
}
else if(total == 50) {
	result.className += "alert alert-info";
	result.innerHTML = "<strong>Pass!</strong> You have just passed the exam with score "+total+"%";
}
else {
	result.className += "alert alert-danger";
	result.innerHTML = "<strong>Fail!</strong> Unfortuanetly, You didn't pass the exam";
}

window.setTimeout(function() {
location.href = document.getElementsByClassName("container")[0].getElementsByTagName("a")[0].href;
}, 5000);

</script>