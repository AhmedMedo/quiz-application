<?php
// in this we will include the files of functions and check if the user is instructor
// we will assume the user is logged in
//include('includes/functions.php');
//echo $_SESSION['quiz_id'];

 include("layout/adminheader.php");

    if(isset($_SESSION['class_id'])) 
    {
      redirect_to('student.php');
    }
//dbconnect("localhost", "quizuser", "quiz", "quiz");
    $row=get_quiz($_GET['edit']);
    // make update
    if(isset($_POST['update']))
    {
      $subject_id=$_POST['subject_id'];
    $title=$_POST['title'];
    $duration=$_POST['duration'];
    $number_of_questions=$_POST['num_of_ques'];
    $status=$_POST['status'];
    //print_r($_POST);
    update_quiz($_GET['edit'],$subject_id,$title,$duration,$number_of_questions,$status);
    //redirect_to('allquestions.php?qid='.$_GET['edit']);

    }

    if(isset($_POST['delete_question']))
    {
      delete_question($_GET['quid']);
      redirect_to('edit.php?edit='.$_GET['edit']);
    }

?>

<div class="container">
<a href="question.php?qid=<?php echo $_GET['edit'];?>"><i class="fa fa-question fa-lg fa-fw"></i>Add new question</a>

    <h1>update quiz</h1>
    <form class="form-horizontal" method="POST" action="edit.php?edit=<?php echo $_GET['edit'];?>">
    <div class="form-group">
         <label  class="col-sm-2 control-label">title</label>
         <div class="col-sm-5">
         <input type="text" name="title" class="form-control" value="<?php echo $row['title'];?>">
         </div>


    </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">quiz subject</label>
    <div class="col-sm-5">
    <select class="form-control" name="subject_id">
      
   
    <?php 
      $result_set=all_subjects($_SESSION['user_id']);
      while ($row_sub=mysqli_fetch_assoc($result_set)): 
        ?>      
      <option value="<?php echo $row_sub['id'];?>" <?php if($row['Subject_id']==$row_sub['id']){echo 'selected="selected"';}?>><?php echo $row_sub['name'];?></option>
    <?php  endwhile;?>
      
    
     </select>
    </div>
  </div>

  <div class="form-group">
  <label class="col-sm-2 control-label">Duration</label>
  <div class="col-sm-5">
        <input type="number" class="form-control" name="duration" value="<?php echo $row['time'];?>">

  </div>
</div>

  
  <div class="form-group">
  <label class="col-sm-2 control-label"> Number of question</label>
    <div class="col-sm-5"> 
      <input type="number" class="form-control" name="num_of_ques" value="<?php echo $row['nuber_of_questions'];?>">

    </div>
    
  </div>

  <div class="form-group">
  <label class="col-sm-2 control-label">status</label>
  <div class="col-sm-5">
  <select class="form-control" id="sel1" name="status">
    <option value="1" <?php if($row['published']==1){echo 'selected="selected"';}?>>publish now</option>
    <option  value="0"<?php if($row['published']==0){echo 'selected="selected"';}?>>draft</option>


  
  </select>
  </div>
</div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="update">update</button>
    </div>
  </div>
</form>


<table class="table table table-striped">
<thead>
  <th> question number</th>
  <th> edit</th>
  <th> delete</th>
</thead>
<tbody>
  <?php
  $i=1;
   $result=questions_of_quiz($_GET['edit']);
  while($quesRow=mysqli_fetch_assoc($result)):?>
  <tr>
    <td><?php echo $i;?></td>
    <td><a href="editquestion.php?qid=<?php echo $_GET['edit'];?>&quid=<?php echo $quesRow['id'];?>&qnum=<?php echo $i;?>">edit</a></td>
    <td><form method="post" action="edit.php?edit=<?php echo $_GET['edit']; ?>&quid=<?php echo $quesRow['id'];?>"><input type="submit" name="delete_question" class="btn btn-danger" value="Delete"/></form></td>




  </tr>
<?php $i++; endwhile;?>
</tbody>
</table>

</div>

  <?php include ('layout/footer.php'); ?>