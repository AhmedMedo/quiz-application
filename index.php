<!--     this page will be the login page where the user will enter his credentials     -->
<?php
include('includes/functions.php');

if(isset($_SESSION['class_id'])) { redirect_to('student.php'); }
if(empty($_SESSION['class_id']) && isset($_SESSION['user_id']) ) 
{ redirect_to('admin.php'); }

  ob_start(); // Initiate the output buffer
  if ( isset( $_POST['submit'] ))
  {
  	  $email = $_POST["email"];
  	  $password =$_POST["password"];
  	  $type =$_POST['type'];
  	  login($email,$password,$type);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Quiz app</title>

    <link href="layout/bootstrap.min.css" rel="stylesheet">
        <link href="layout/signin.css" rel="stylesheet">

  <script src="layout/jquery-1.11.3.min.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="quizIcon center" style = "background-image: url('layout/images/Quiz.png'); width:200px; height:200px; background-size:cover; margin:0 auto;"></div>
      <form class="form-signin" method="POST" action="index.php" autocomplete="off">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
           <label style="margin-right:20px;">login as</label>
           <label class="radio-inline">
			  <input type="radio" name="type" id="inlineRadio1" value="instructor"> instructor
			</label>
			<label class="radio-inline">
			  <input type="radio" name="type" id="inlineRadio2" value="student"> student
			</label>

        <button class="btn btn-lg btn-primary btn-block" type="submit" style="margin-top:15px;" name="submit">Sign in</button>
      </form>

      <?php if(isset($_SESSION['login_error']))
      {?>
           <div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong>  <?php echo $_SESSION['login_error'];?>
</div>
           
<?php }?>
     <!-- /container -->
</div>
  <script src="layout/jquery-1.11.3.min.js"></script>
    <script src="layout/bootstrap.min.js"></script>

  </body>
</html>
<?php closeconnect(); ?>