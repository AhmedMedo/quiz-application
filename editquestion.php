<?php

// in this we will include the files of functions and check if the user is instructor
// we will assume the user is logged in
//include('includes/functions.php');
//dbconnect("localhost", "quizuser", "quiz", "quiz");
 include("layout/adminheader.php");

$question_id=$_GET['quid'];

$row=get_question($question_id);
$choices=get_choices($question_id);
$array_of_choices=array();
$choices_id=array();
while($rows=mysqli_fetch_assoc($choices))
{
	array_push($array_of_choices,$rows['content']);
	array_push($choices_id,$rows['id']);
}
//print_r($array_of_choices[]);
// in add press the data will be saved and send to database
if(isset($_POST['update']))
{
 // save it to database with the current quiz id
// unset($_SESSION['question_error']);
 $content=$_POST['content'];
 $correct=$_POST['correct_answer'];
 $choice_1=$_POST['choice_1'];
 $choice_2=$_POST['choice_2'];
 $choice_3=$_POST['choice_3'];
 $choice_4=$_POST['choice_4'];
 //$choices=array();
 //print_r($_POST);
 if($correct==1){$correct=$choice_1;}

 if($correct==2){$correct=$choice_2;}

 if($correct==3){$correct=$choice_3;}

 if($correct==4){$correct=$choice_4;}

update_question($content,$correct,$question_id);
update_choice($choices_id[0],$choice_1,$question_id);
update_choice($choices_id[1],$choice_2,$question_id);
update_choice($choices_id[2],$choice_3,$question_id);
update_choice($choices_id[3],$choice_4,$question_id);
redirect_to('edit.php?edit='.$_GET['qid']);


  //add_question($content,$correct,$choice_1,$choice_2,$choice_3,$choice_4);

 //array_push($choices, $choice1,$choice2,$choice3,$choice4);
 //print_r($choices);


}


?>

<div class="container">



   <form method="POST" action="editquestion.php?qid=<?php echo $_GET['qid']?>&quid=<?php echo $question_id;?>&qnum=<?php echo $_GET['qnum'];?>">
  <div class="form-group">
    <label for="exampleInputEmail1">Question <?php echo $_GET['qnum'];?></label>
      <textarea class="form-control" rows="5" name="content" ><?php echo $row['content'];?></textarea>

  </div>
  




<div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 1 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice one" name="choice_1" value="<?php echo $array_of_choices[0];?>" required> 
      </div>
    </div>
 </div>
 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 2 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice two"name="choice_2" value="<?php echo $array_of_choices[1];?>" required> 
      </div>
    </div>
 </div>
 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 3 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice three" name="choice_3" value="<?php echo $array_of_choices[2];?>"required>
      </div>
    </div>
 </div>
 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Choice 4 </label>
      </div>
      
      <div class="col-sm-5">
      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Choice Four" name="choice_4" value="<?php echo $array_of_choices[3];?>" required> 
      </div>
    </div>
 </div>

 <div class="form-group">
  
    <div class="row">
    <div class="col-sm-2">
      <label for="exampleInputPassword1">Correct answer</label>
      </div>
      
      <div class="col-sm-5">
      <select name="correct_answer" class="form-control" required>
      <option value="1"<?php if($array_of_choices[0]==$row['right_answer']){echo 'selected="selected"';}?>>choice 1</option>
      <option value="2" <?php if($array_of_choices[1]==$row['right_answer']){echo 'selected="selected"';}?>>choice 2</option>
      <option value="3" <?php if($array_of_choices[2]==$row['right_answer']){echo 'selected="selected"';}?>>choice 3</option>
      <option value="4" <?php if($array_of_choices[3]==$row['right_answer']){echo 'selected="selected"';}?>>choice 4</option>


</select>
      </div>
    </div>
 </div>
  
  <br>
  <div class="row">
  <div class="col-sm-5">
  <button type="submit" class="btn btn-primary" name="update">update</button>
  </div>
   <div class="col-sm-5">
  </div>
  </div>
</form>

</div>

  <?php include ('layout/footer.php'); ?>