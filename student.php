<?php
 include("layout/header.php");
 
 if(empty($_SESSION['class_id']))
 {
 	redirect_to('index.php');
 }
 
?>

<script type="text/javascript">
	function startQuiz(url){
		window.location = url;
	}
</script>

<div class="container">

		  <div class="available_quiz">
		    <h2>Available Quizzes</h2>
		    <table class="table table-hover">
		     <thead>
		        <tr>
		          <th>Date</th>
		          <th>Name</th>
		          <th>Subject</th>
		          <th>Duration</th>
		          <th>Take quiz</th>
		        </tr>
		      </thead>
		      <tbody>
		         <?php $result=unsolved_quizzes($_SESSION['user_id']);
		          while ($row=mysqli_fetch_assoc($result)) :?>     
		          <tr>
		            <td><?php echo $row['date'];?></td>
		            <td><?php echo $row['title'];?></td>
		            <td><?php echo $row['subject'];?></td>
		            <td><?php echo $row['time'];?> Mins.</td>
		            <td><button class= "btn btn-info" onClick="startQuiz('takequiz.php?qid=<?php echo $row['quiz'];?>&t=<?php echo $row['time'];?>&name=<?php echo $row['title'];?>')">Start</button></td>
		          </tr>
		        <?php endwhile;?>
		      </tbody>
		    </table>
		  </div>


		  <div class="past_quiz">
		    <h2>Past Quizzes</h2>
		    <?php if(mysqli_num_rows(past_quizzes($_SESSION['user_id'])) == 0 ){
		    	echo "<i>You haven't solved any quizzes yet!<i>"; }
		    	else {
		    ?>
		    <table class="table table-hover">
		     <thead>
		        <tr>
		          <th>Date</th>
		          <th>Name</th>
		          <th>Subject</th>
		          <th>Degree</th>
		        </tr>
		      </thead>
		      <tbody>
		         <?php $result=past_quizzes($_SESSION['user_id']);

		          while ($row=mysqli_fetch_assoc($result)) : ?>     
		          <tr>
		            <td><?php echo $row['Date_taken'];?></td>
		            <td><?php echo $row['title'];?></td>
		            <td><?php echo $row['Subject'];?></td>
		            <td><?php echo $row['result'];?> / <?php echo $row['questions'];?></td>
		          </tr>
		        <?php endwhile;?>
		      </tbody>
		    </table>
		    <?php } ?>
		  </div>
</div>


<script type="text/javascript">
 
 var past_quiz_tr = document.getElementsByClassName("past_quiz")[0].getElementsByTagName("table")[0].getElementsByTagName("tbody")[0].getElementsByTagName("tr");
 	for (var i =0; i < past_quiz_tr.length ; i++) {
 		var total_array = past_quiz_tr[i].getElementsByTagName("td")[3].innerHTML.split(" ")
 		var total = parseInt(total_array[0])/parseInt(total_array[2])*100;
 		if(total > 50) {
 			past_quiz_tr[i].className = "success";
 		}
 		else if(total == 50) {
 			past_quiz_tr[i].className = "warning";
 		}
 		else {
 			past_quiz_tr[i].className = "danger";
 	}
 };
 
</script>

<?php include ('layout/footer.php'); ?>
